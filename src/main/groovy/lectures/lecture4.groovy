package lectures

/**
 * Created by Pavel on 08.11.2014.
 */

// Categories
class Pouncer {
    static pounces (Integer self ) {
        (0..<self).inject("") { s, n ->
            s += "boing! "
        }
    }
}

use (Pouncer) {
    assert 3.pounces() == "boing! boing! boing! "
}

