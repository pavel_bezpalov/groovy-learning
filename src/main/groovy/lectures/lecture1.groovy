package lectures
/**
 * Created by Pavel Bezpalov on 20.10.2014.
 */
import groovy.transform.ToString

// GString
def gstring = "GString"
def success = "successfully tested!"
println "$gstring $success"

// OOP
class Greet {
    def var

    Greet(who) {
        var = who[0].toUpperCase() + who[1..6] + who[7].toUpperCase() + who[8..-1]
    }

    def salute() {
        println "OOP $var tested!"
    }
}

g = new Greet('successfully')
g.salute()

// Ranges
def ranges = (1..10).collect { it * 2 }
println ranges
println "Ranges $success"

// Star operator
def starop = ['cat', 'elephant']*.size()
println starop
println "Star operator $success"

// Constructor
@ToString
class User {
    def name
    def age
}

User user1 = new User(name: 'Petya', age: 20)
println user1

User user2 = [name: 'Vasya', age: 25] as User
println user2

User user3 = new User()
user3.with {
    name = 'Kolya'
    age = 30
}
println user3
println "Constructor $success"

/* Result:
GString successfully tested!
OOP SuccessFully tested!
[2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
Ranges successfully tested!
[3, 8]
Star operator successfully tested!
lectures.User(Petya, 20)
lectures.User(Vasya, 25)
lectures.User(Kolya, 30)
Constructor successfully tested!
 */