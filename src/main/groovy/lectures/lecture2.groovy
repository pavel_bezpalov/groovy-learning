package lectures

/**
 * Created by Pavel Bezpalov on 24.10.2014.
 */

//Everything is an Object
(60 * 60 * 24 * 365).toString()

assert "abc" - "a" == "bc"

assert 'ABCDE'.indexOf(67) == 2

1.plus(1)

// Operator Overriding
class User2 {
    String name
    User2(name) {
        this.name = name
    }

    public plus(def a) {
        println name + a
    }
}

def u = new User2('Vasya')
u + ' super'

// Numbers
assert 42I == new Integer("42")
assert 123L == new Long("123")
assert 2147483648 == new Long("2147483648")
assert 456G == new java.math.BigInteger("456")
assert 123.45 == new java.math.BigDecimal("123.45")
assert 1.200065D == new Double("1.200065")
assert 1.234F == new Float("1.234")
assert 1.23E23D == new Double("1.23E23")

// Groovy Truth
assert true
assert ![]
assert !""
assert !0
assert !null

// Regular Expressions
assert 'abc' ==~ /abc/
assert 'abc' ==~ /a.c/

def m = ( 'mistlemuscle' =~ /m(.)s(.)le/)
assert m.size() == 2
assert m[0] == ['mistle', 'i', 't']
assert m[0].size() == 3 && m[0][0] == 'mistle' && m[0][1] == 'i' && m[0][2] == 't'
assert m[1] == ['muscle', 'u', 'c']
assert m[1].size() == 3 && m[1][0] == 'muscle' && m[1][1] == 'u' && m[1][2] == 'c'

"abc".replaceAll(/(a) (b) (c)/, '$1$3')
/* Result: abc */

// Closures
def clos = { println "hello!"}
clos()
/* Result: hello! */

def printSum = {a, b -> print a+b}
printSum( 5, 7)
/* Result: 12 */

def myConst = 5
def incByConst = {num -> num + myConst }
println incByConst(10)
/* Result: 15 */

class Class1 {
    def closure = {
        println this.class.name
        println delegate.class.name
        def nestedClos = {
            println owner.class.name
        }
        nestedClos()
    }
}
def closs = new Class1().closure
closs.delegate = this
closs()
/* Result:
lectures.Class1
lectures.lecture2
lectures.Class1$_closure1
 */