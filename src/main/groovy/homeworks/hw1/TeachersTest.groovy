package homeworks.hw1

/**
 * Created by Pavel Bezpalov on 20.10.2014.
 */

// Teacher's example of testing Elvis operator
class TeachersTest extends GroovyTestCase {
    String firstOption = "first"
    String secondOption = "second"
    String actual
    String expected

    void test1() {

        actual = true ? firstOption : secondOption
        expected = firstOption
        assert actual == expected

    }

    void test2() {
        actual = false ? firstOption : secondOption
        expected = secondOption
        assert actual == expected
    }

    void test3() {
        actual = firstOption ?: secondOption
        expected = firstOption
        assert actual == expected
    }

    void test4() {
        actual = null ?: secondOption
        expected = secondOption
        assert actual == expected
    }
}
