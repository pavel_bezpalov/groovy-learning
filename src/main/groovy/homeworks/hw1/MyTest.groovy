package homeworks.hw1

/**
 * Created by Pavel Bezpalov on 20.10.2014.
 * My Homework 1
 * Unit tests for Groovy features
 */

class MyTest extends GroovyTestCase {
    def actual
    def expected

    // Testing Java String and Groovy String
    void test1() {
        def javastring = 'this is javastring'

        actual = '$javastring'
        expected = 'this is javastring'
        assert actual == expected: "Java String not allowed params"

    }

    void test2() {
        def groovystring = "this is groovystring"

        actual = "$groovystring"
        expected = "this is groovystring"
        assert actual == expected

    }

    // Testing Strings math operations
    void test3() {
        def str = 'a'

        actual = ((str * 1 + 'bc')++)--
        expected = 'abc'

        assert actual == expected
    }
    // Primitive types as objects in Groovy
    void test4() {
        actual = 1.plus(1).power(3).multiply(6).minus(6)
        expected = 42

        assert actual == expected
    }
    // Dynamic Typing and Optional Return Type
    def method() {
        def var = 10
        var = "abcd"
    }

    void test5() {
        actual = method()
        expected = "abcd"

        assert actual == expected
    }
}
