package homeworks.hw2

/**
 * Created by Pavel Bezpalov on 30.10.2014.
 * Homework 2
 * Write script which will process files in folder (folder path should be passed to script as a parameter).
 - each file which name started with "a_" should calculate number of "a" character in it and write result
 as a new line to file
 - each file which starts from "1_" should replace file content with randomly generated matrix 4 x 4
 and sum of main diagonal and antidiagonal from new line
 - each file which starts from "d_" should replace file content with date in "YYYY-MM-DD HH-MM-SS" format
 - other file should be ignored

 All processed files should be renamed with next format: "done_" + old name

 Helpful materials:
 http://groovy.codehaus.org/JN2015-Files
 http://mrhaki.blogspot.com/2009/09/groovy-goodness-parsing-commandline.html
 */

list = []
new File("${args[0]}").eachFile { list << it.name }

closure_1 = {
    file = new File("${args[0]}\\$it")
    array = new int[4][4]
    seed = System.currentTimeMillis()
    random = new Random(seed)
    diagonalsum = 0
    antidiagonalsum = 0
    file.write("{ ")
    for (i in 0..3) {
        file.append("{ ")
        for (j in 0..3) {
            array[i][j] = random.nextInt()
            file.append("${array[i][j]}")
            (j == 3) ? file.append(" ") : file.append(", ")
            if (i == j) diagonalsum += array[i][j]
            if (i + j == 3) antidiagonalsum += array[i][j]
        }
        i == 3 ? file.append("} }") : file.append("},\n")
    }
    file.append("\n$diagonalsum")
    file.append("\n$antidiagonalsum")
    file.renameTo(new File("${args[0]}\\done_$it"))
}

closure_a = {
    file = new File("${args[0]}\\$it")
    pattern = ~/[a]/
    matcher = pattern.matcher(file.getText())
    file.append("\n${matcher.getCount()}")
    file.renameTo(new File("${args[0]}\\done_$it"))
}

closure_d = {
    file = new File("${args[0]}\\$it")
    currenttime = System.currentTimeMillis()
    file.write("${String.format('%tY-%<tm-%<td %<tH-%<tM-%<tS', new Date(currenttime))}")
    file.renameTo(new File("${args[0]}\\done_$it"))
}

list.each {
    switch (it) {
        case ~/1_.*?/: closure_1(it)
            break
        case ~/a_.*?/: closure_a(it)
            break
        case ~/d_.*?/: closure_d(it)
            break
    }
}
