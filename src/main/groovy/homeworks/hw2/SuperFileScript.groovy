package homeworks.hw2

import homeworks.hw4.WithLogging

/**
 * Created by Pavel on 12.11.2014.
 */
@WithLogging
def listing(path) {
    list = []
    new File("$path").eachFile { list << it.name }
    return list
}
@WithLogging
def writeAndRename(path, file, textbuffer) {
    new File("$path/$file").write("""$textbuffer""")
    new File("$path/$file").renameTo(new File("$path/done_$file"))
}
@WithLogging
def process1Files() {
    textBuffer1 = ""
    array = new int[4][4]
    seed = System.currentTimeMillis()
    random = new Random(seed)
    diagonalsum = 0
    antidiagonalsum = 0
    for (i in 0..3) {
        for (j in 0..3) {
            array[i][j] = random.nextInt()
            textBuffer1 += "${array[i][j]}"
            (j == 3) ?: (textBuffer1 += " ")
            if (i == j) diagonalsum += array[i][j]
            if (i + j == 3) antidiagonalsum += array[i][j]
        }
        i == 3 ?: (textBuffer1 += "\n")
    }
    textBuffer1 += "\n$diagonalsum\n$antidiagonalsum"
    return textBuffer1
}
@WithLogging
def processDFiles() {
    currenttime = System.currentTimeMillis()
    textBufferD = "${String.format('%tY-%<tm-%<td %<tH-%<tM-%<tS', new Date(currenttime))}"
    return textBufferD
}
@WithLogging
def processAFiles(path, file) {
    processingFile = new File("$path\\$file")
    pattern = ~/[a]/
    content = processingFile.getText()
    textBufferA = content
    matcher = pattern.matcher(content)
    textBufferA += "\n${matcher.getCount()}"
    return textBufferA
}

listing(args[0]).each {
    switch (it) {
        case ~/1_.*?/: writeAndRename(args[0], it, process1Files())
            break
        case ~/a_.*?/: writeAndRename(args[0], it, processAFiles(args[0], it))
            break
        case ~/d_.*?/: writeAndRename(args[0], it, processDFiles())
            break
    }

}

