package homeworks.hw2

import spock.lang.Specification

/**
 * Created by Pavel on 11.11.2014.
 */

class SuperTests extends Specification {

    def "receiving list of files in folder"() {
        setup:
        def folder = new File("$path")
        folder.mkdir()
        def file = new File("$path", 'file.txt')
        def file_1 = new File("$path", '1_file.txt')
        def file_a = new File("$path", 'a_file.txt')
        def file_d = new File("$path", 'd_file.txt')
        [file, file_1, file_a, file_d].each { it.createNewFile() }
        expect:
        "${listing(path)}" == "[1_file.txt, a_file.txt, d_file.txt, file.txt]"
        cleanup:
        [file, file_1, file_a, file_d, folder].each { it.delete() }
        where:
        listing = new SuperFileScript().&listing
        path = "D:/tests"
    }

    def "write text buffer and rename files"() {
        setup:
        def folder = new File("$path")
        folder.mkdir()
        def file = new File("$path", "$testfile")
        def doneFile = new File("$path", 'done_file.txt')
        file.createNewFile()
        writeAndRename(path, testfile, textbuffer)
        expect:
        !file.exists()
        doneFile.exists()
        doneFile.getText() == textbuffer
        cleanup:
        [file, folder, doneFile].each { it.delete() }
        where:
        writeAndRename = new SuperFileScript().&writeAndRename
        path = "D:/tests"
        testfile = 'file.txt'
        textbuffer = """2343242342  23423432 34242234
dsfafdafdafa
3432423423"""
    }

    def "processing files starting with 1_"() {
        expect:
        process1Files() ==~ pattern
        where:
        process1Files = new SuperFileScript().&process1Files
        pattern = /((-?)\d+ (-?)\d+ (-?)\d+ (-?)\d+\n){4}(-?)\d+\n(-?)\d+/
    }

    def "processing files starting with d_"() {
        expect:
        processDFiles() ==~ pattern
        where:
        processDFiles = new SuperFileScript().&processDFiles
        pattern = /(\d+-){2}\d+ (\d+-){2}\d+/
    }

    def "processing files starting with a_"() {
        setup:
        def folder = new File("$path")
        folder.mkdir()
        def file = new File("$path", "$testFile")
        file.createNewFile()
        file.write("""afdafgaaadfaf
afafadfa
dasdasd
36346
346346346
34636""")
        expect:
        processAFiles(path, testFile) ==~ pattern
        cleanup:
        [file, folder].each { it.delete() }
        where:
        processAFiles = new SuperFileScript().&processAFiles
        pattern = /(?m)(^.*\n)*\d+$/
        path = "D:/tests"
        testFile = 'file.txt'
    }

}