You can't be Agile if you Code sucks.

Programs must be written for people to read.

Care about design of your code:
    good names for variables, methods, ...
    short method, smaller classes ...
    learn by reading good code.

Keep it simple

Write tests with high coverage
Write all your tests before checkin
Checkin Frequently

Avoid shortcuts

Team should own code not one person

Promote positive interaction

Provide constructive feedback

Code review

Threat Warning as Errors

Keep comments minimum


