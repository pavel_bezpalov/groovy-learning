package homeworks.hw4

/**
 * Created by Pavel on 13.11.2014.
 */

import java.lang.annotation.Retention
import java.lang.annotation.Target
import org.codehaus.groovy.transform.GroovyASTTransformationClass
import java.lang.annotation.ElementType
import java.lang.annotation.RetentionPolicy

@Retention(RetentionPolicy.SOURCE)
@Target([ElementType.METHOD])
@GroovyASTTransformationClass(["homeworks.hw4.LoggingASTTransformation"])
public @interface WithLogging {
}
