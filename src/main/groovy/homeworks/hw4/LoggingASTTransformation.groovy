package homeworks.hw4

import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.builder.AstBuilder
import org.codehaus.groovy.ast.expr.ArgumentListExpression
import org.codehaus.groovy.ast.expr.ConstantExpression
import org.codehaus.groovy.ast.expr.MethodCallExpression
import org.codehaus.groovy.ast.expr.VariableExpression
import org.codehaus.groovy.ast.stmt.ExpressionStatement
import org.codehaus.groovy.ast.stmt.ReturnStatement
import org.codehaus.groovy.ast.stmt.Statement
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

import static org.codehaus.groovy.ast.tools.GeneralUtils.*

/**
 * This transformation finds all the methods defined in a script that have
 * the @WithLogging annotation on them, and then weaves in a start, method's
 * parameters with their values and stop message that is logged using println.
 *
 * @author Hamlet D'Arcy
 * @author Pavel Pezpalov
 * original source: https://github.com/groovy/groovy-core/tree/master/src/examples/transforms/local
 */
@GroovyASTTransformation(phase = CompilePhase.CANONICALIZATION)
public class LoggingASTTransformation implements ASTTransformation {

    public void visit(ASTNode[] nodes, SourceUnit sourceUnit) {
        // find all methods annotated with @WithLogging
        if (nodes.length != 2) return
        if (nodes[0] instanceof AnnotationNode && nodes[1] instanceof MethodNode) {
            nodes[1].each { MethodNode method ->
                Statement startMessage = createPrintlnAst("Starting $method.name")
                Statement endMessage = createPrintlnAst("Ending $method.name")

                List existingStatements = method.getCode().getStatements()
                existingStatements.add(0, startMessage)

                method.parameters.eachWithIndex { param, index ->
                    if (param) {
                        Statement paramMessage = createPrintAst("$param.name = ")
                        Statement valueMessage = createPrintlnValAst("${param.name}")
                        index *= 2
                        existingStatements.add(index + 1, paramMessage)
                        existingStatements.add(index + 2, valueMessage)

                    }
                }

                def checkForReturn = existingStatements.last()
                if (checkForReturn in ReturnStatement) {
                    int lastIndex = existingStatements.indexOf(checkForReturn)
                    existingStatements.add(lastIndex,endMessage)
                    }
                else existingStatements.add(endMessage)
            }
        }
    }
    // Native ASTTree
    private static Statement createPrintlnAst(String message) {
        return new ExpressionStatement(
                new MethodCallExpression(
                        new VariableExpression("this"),
                        new ConstantExpression("println"),
                        new ArgumentListExpression(
                                new ConstantExpression(message)
                        )
                )
        )
    }
    // GeneralUtils
    private static Statement createPrintAst(String message) {
        return stmt(
                callX(
                        varX("this"),
                        constX("print"),
                        args(
                                constX(message)
                        )
                )
        )
    }
    // AstBuilder
    private static Statement createPrintlnValAst(String message) {
        List<Statement> statements = new AstBuilder().buildFromSpec {
            expression {
                methodCall {
                    variable 'this'
                    constant 'println'
                    argumentList {
                        variable message
                    }
                }
            }
        }
        return statements[0]
    }
}