package homeworks.hw4

/**
 * Created by Pavel on 20.11.2014.
 */
import spock.lang.Specification

class WithLoggingTest  extends Specification {
    class Bunny {
        @WithLogging
        def simpleMethod() {
        }

        @WithLogging
        def methodWithParameter(message) {
        }

        @WithLogging
        def methodWith2Parameters(message, count) {
        }

        @WithLogging
        def methodWith2ParametersAndReturn(message, count) {
            return message
        }

    }
    def bunny = new Bunny()

    void "simple method"() {
        given:
        def standardOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(standardOut));

        bunny.simpleMethod()

        expect:
        def result = standardOut.toString("ISO-8859-1").split('\\n')
        assert "Starting simpleMethod" == result[0].trim()
        assert "Ending simpleMethod" == result[1].trim()
    }

    void "method with parameter"() {
        given:
        def standardOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(standardOut));

        bunny.methodWithParameter("Hi All!")

        expect:
        def result = standardOut.toString("ISO-8859-1").split('\\n')
        assert "Starting methodWithParameter" == result[0].trim()
        assert "message = Hi All!" == result[1].trim()
        assert "Ending methodWithParameter" == result[2].trim()
    }

    void "method with 2 parameters"() {
        given:
        def standardOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(standardOut));

        bunny.methodWith2Parameters("Hi All!", 2)

        expect:
        def result = standardOut.toString("ISO-8859-1").split('\\n')
        assert "Starting methodWith2Parameters" == result[0].trim()
        assert "message = Hi All!" == result[1].trim()
        assert "count = 2" == result[2].trim()
        assert "Ending methodWith2Parameters" == result[3].trim()
    }

    void "method with 2 parameters and return"() {
        given:
        def standardOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(standardOut));

        bunny.methodWith2ParametersAndReturn("Hi All!", 2)

        expect:
        def result = standardOut.toString("ISO-8859-1").split('\\n')
        assert "Starting methodWith2ParametersAndReturn" == result[0].trim()
        assert "message = Hi All!" == result[1].trim()
        assert "count = 2" == result[2].trim()
        assert "Ending methodWith2ParametersAndReturn" == result[3].trim()
    }
}