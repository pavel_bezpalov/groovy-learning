package Homestudy.bgg.c2;

/**
 * Created by Pavel Bezpalov on 23.10.2014.
 * A Sample Java File
 */
public class SayHello {
    public static void main(String args[]) {
        Name name = new Name();
        name.setFirstName(args[0]);
        System.out.println(name.toString());
    }
}
/* Result executed with param "Luke":
Hello Luke, Java calling Groovy
 */