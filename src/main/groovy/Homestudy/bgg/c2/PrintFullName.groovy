package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * A Script Function
 */

def PrintFullName(firstName, lastname) {
    println "$firstName $lastname"
}
PrintFullName("Luke","Skywalker")
PrintFullName("Darth", "Vader")
/* Result:
Luke Skywalker
Darth Vader
 */