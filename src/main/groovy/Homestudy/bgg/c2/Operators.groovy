package Homestudy.bgg.c2

/**
 * Created by Pavel on 04.11.2014.
 */

//Spread operator
def map = [1:"Vishal",2:"Chris",3:"Joseph",4:"Jim"]
def keys = [1,2,3,4]
def values = ["Vishal","Chris","Joseph","Jim"]
assert map*.key == keys
assert map*.value == values

//Elvis operator
def firstName = user.firstName == null ? "unknown" : user.firstName // Java ternary
def firstName2 = user.firstName ?: "unknown" // Groovy Elvis

//Using the Safe Navigation/Dereference Operator
class User {
    String firstName
    String lastName
    def printFullName = {
        println "${firstName} ${lastName}"
    }
}
User user
println "Groovy FirstName = ${user?.firstName}"

//Using the Field Operator
class Todo {
    String name
    def getName() {
        println "Getting Name"
        name
    }
}
def todo = new Todo(name: "Jim")
println todo.name
println todo.@name

//Using the Method Closure Operator
def list = ["Chris","Joseph","Jim"]
// each takes a closure
list.each { println it }
String printName(String name) {
    println name
}
// & causes the method to be accessed as a closure
list.each(this.&printName)

//Using the Diamond Operator
List<List<String>> list1 = new ArrayList<List<String>>()
List<List<String>> list2 = new ArrayList<>()
assert list1 == list2