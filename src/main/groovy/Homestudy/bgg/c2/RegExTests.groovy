package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 24.10.2014.
 * Testing Regular Expression Operators
 */
/*
// Match Operator
    assert "abc" ==~ 'abc'
    assert "abc" ==~ /abc/
    assert "abcabc" ==~ /abc/ // Fails
    assert "abc" ==~ /^a.c/
    assert "abc" ==~ /^a../
    assert "abc" ==~ /.*c$/
    assert "abc" ==~ ".*c\$" // Slashy string is better

// Find Operator
    def winpath = /C:\windows\system32\somedir/
    def matcher = winpath =~ /(\w{1}):\\(\w+)\\(\w+)\\(\w+)/
    println matcher // java.util.regex.Matcher[pattern=(\w{1}):\\(\w+)\\(\w+)\\(\w+) region=0,27 lastmatch=]
    println matcher[0] // [C:\windows\system32\somedir, C, windows, system32, somedir]
    println matcher[0][1] // C
    def newPath = matcher.replaceFirst('/etc/bin/')
    println newPath // /etc/bin/

// Pattern Operator
    def saying = """Now is the time for all good men ( and women) to come to the aid
of their country"""
    def pattern = ~/(\w+en)/
    def matcher = pattern.matcher(saying)
    def count = matcher.getCount()
    println "Matches = $count"
    for (i in 0..<count) {
        println matcher[i]
    }

// Validating a Phone Number
def phoneValidation = /^[01]?\s*[\(\.-]?(\d{3})[\)\.-]?\s*(\d{3})[\.-](\d{4})$/
assert '(800)555-1212' ==~ phoneValidation
assert '1(800) 555-1212' ==~ phoneValidation
assert '1-800-555-1212' ==~ phoneValidation
assert '1.800.555.1212' ==~ phoneValidation
*/

// Parsing the Phone Number
class Phone {
    String areaCode
    String exchange
    String local
}
def phoneStr = '(800)555-1212'
def phoneRegex = ~/^[01]?\s*[\(\.-]?(\d{3})[\)\.-]?\s*(\d{3})[\.-](\d{4})$/
def matcher = phoneRegex.matcher(phoneStr)
def phone = new Phone(
        areaCode: matcher[0][1],
        exchange: matcher[0][2],
        local: matcher[0][3])
        println "Original Phone Number: ${phoneStr}"
        println """Parsed Phone\
\n\tArea Code = ${phone.areaCode}\
\n\tExchange = ${phone.exchange}\
\n\tLocal = ${phone.local}"""