package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 02.11.2014.
 */

def method(bool) {
    try {
        if (bool) throw new Exception("foo")
        1
    } catch(e) {
        2
    } finally {
        3
    }
}

assert method(false) == 1
assert method(true) == 2

/* Multi-catch Block
try {
 ...
} catch(IOException | NullPointerException e) {
    one block to handle 2 exceptions
}
 */