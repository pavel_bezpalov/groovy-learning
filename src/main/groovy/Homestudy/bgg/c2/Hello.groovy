package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * A Simple Groovy Script: Hello.groovy
 */
println "Hello ${args[0]}, may Groovy be with you."

/* Result executed with parameter "Luke Skywalker":
Hello Luke Skywalker, may Groovy be with you.
 */