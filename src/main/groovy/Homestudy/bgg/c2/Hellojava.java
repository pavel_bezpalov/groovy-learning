package Homestudy.bgg.c2;

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * The Java Version: HelloJava.java
 */
public class Hellojava {
    public static void main(String... args) {
        System.out.println("Hello " + args[0] + ", may Java be with you.");
    }
}
/* Result executed with parameter "Luke Skywalker":
Hello Luke Skywalker, may Java be with you.
 */