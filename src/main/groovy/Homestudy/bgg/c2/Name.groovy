package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 23.10.2014.
 * A Sample Groovy File
 */
class Name {
    String firstName

    String toString() { return "Hello ${firstName}, Java calling Groovy" }
}
