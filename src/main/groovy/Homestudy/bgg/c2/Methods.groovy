package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 02.11.2014.
 */

// Defining a Method the Java Way
public String hello(String name) {
    return "Hello,  " + name;
}

//Defining a Method Using the Groovy Idiom
def hello(name) {
    "Hello, $name"
}