package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 01.11.2014.
 */

//Groovy Truth
/* //Null Check for Collections and Strings in Java
if (collection != null && !collection.isEmpty()) {...} // collection
if (str != null && str.length() > 0) {...} //string

//Null Check for Collections and Strings in Groovy
if (collection) {...} // collection
if (str) {...} // string
*/
//Using asBoolean() Method
class SomeClass {
boolean value
boolean asBoolean() { value }
}

assert new SomeClass(value: true)
assert !new SomeClass(value: false)

//Logical Branching
def x = 3.44
def result = ""

switch (x) {
    case "foo": result = "String"
        break

    case [4, 5, 6]: result = "List"
        break

    case 12..30: result = "Range"
        break

    case Integer: result = "Integer"
        break

    case Number: result = "Number"
        break

    default:
        result = "Default"
}

assert result == "Number"

//Looping
for (i in 'Hello') // Iterate over a string
println i

for (i in 0..10) //Iterate over a range
println i

for (i in [1,2,3,4]) //Iterate over a list
println i

authors = [1:'Vishal', 2:'Jim', 3:'Chris', 4:'Joseph']

for (entry in authors) //Iterate over a map
    println entry.key + ' ' + entry.value