package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 29.10.2014.
 */

// Creating and Using Arrays
def stringArray = new String[3]
println stringArray.size()
stringArray[0] = "Chris"
println stringArray // [Chris, null, null]
stringArray[1] = "Joseph"
stringArray[2] = "Jim"
println stringArray // [Chris, Joseph, Jim]
println stringArray[1] // Joseph
stringArray.each { println it }
/*
Chris
Joseph
Jim
 */
println stringArray[-1..-3] // [Jim, Joseph, Chris]

// Creating and Using Lists
def emptyList = []
println emptyList.class.name // java.util.Arraylist
println emptyList.size // 0

def list = ["Chris"] // List with one item in it
// Add items to the list
list.add "Joseph" // Notice the optional () is missing
list << "Jim" // Notice the overloaded left-shift operator
println list.size // 3

// Iterate over the list
list.each { println it }
/*
Chris
Joseph
Jim
 */
// Access items in the list
println list[1] // Joseph
list[0] = "Christopher"
println list.get(0) // Christopher

list.set(0, "Chris")
println list.get(0) // Chris

list.remove 2
list -= "Joseph"
list.each { println it } // Chris

list.add "Joseph"
list += "Jim"
list.each { println it }
/*
Chris
Joseph
Jim
 */
println list[-1] // Jim

// Creating and Using Maps
def emptyMap = [:]
// map.class returns null, use getClass()
println emptyMap.getClass().name
println emptyMap.size()

def todos = ['a': 'Write the map section', 'b': 'Write the set section']
println todos.size()
println todos["a"]
println todos."a"
println todos.a
println todos.getAt("b")
println todos.get("b")
println todos.get("c", "unknown")
println todos

todos.d = "Write the ranges section"
println todos.d
todos.put('e', 'Write the strings section')
println todos.e
todos.putAt 'f', 'Write the closure section'
println todos.f
todos[null] = 'Nothing set'
println todos[null]

// Print each key/value pair on a separate line
// Note: it is an implicit iterator
todos.each { println "Key: ${it.key}, Value: ${it.value}" }
// Print each key/value pair on separate line with index
todos.eachWithIndex { it, i -> println "${i} Key: ${it.key}, Value: ${it.value}" }
// Print the value set
todos.values().each { println it }

// Creating and Using ranges
def numRange = 0..9
println numRange.size()
numRange.each { print it }
println ""
println numRange.contains(5)

def alphaRange = 'a'..'z'
println alphaRange.size()
println alphaRange[1]

def exclusiveRange = 1..<10
println exclusiveRange.size()
exclusiveRange.each { print it }
println ""
println exclusiveRange.contains(10)

def reverseRange = 9..0
reverseRange.each { print it }

// Iterating with Ranges
println "Java style for loop"
for (int i = 0; i <= 9; i++) {
    println i
}

println "Groovy style for loop"
for (i in 0..9) {
    print i
}

println "Groovy range loop"
(0..9).each { i ->
    print i
}

// Creating and Using Sets
def emptySet = [] as Set
println emptySet.class.name
println emptySet.size()

def list1 = ["Chris", "Chris"]
def set = ["Chris", "Chris"] as Set
println "List Size: ${list1.size()} Set Size: ${set.size()}"
set.add "Joseph"
set << "Jim"
println set.size()
println set

//Iterate over the set
set.each { println it }

set.remove 2
set -= "Joseph"
set.each { println it }
set += "Joseph"
set += "Jim"
set.each { println it }

// Convert a set to a list
list1 = set as List
println list1.class.name
println set.asList().class.name
println set.toList().class.name