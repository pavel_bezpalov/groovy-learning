package Homestudy.bgg.c2

/**
 * Created by Pavel Bezpalov on 02.11.2014.
 */

//Calling a Closure
def closurevar = { println 'Hello world' }
println "closure is not called yet"
println ""
closurevar.call()

//Closure with parameter
def closurevar1 = { param -> println "Hello $param" }
closurevar1.call('world')
closurevar1('implicit world')

//Parameters with default values
def sayhello = { str1, str2 = "default world" -> println "$str1 $str2" }
sayhello("Hello", "world")
sayhello("Hello")

//Using Return Keyword
def sum = { list -> return list.sum() }
assert sum([2, 2]) == 4

//Return Keyword Optinal
def sum1 = { list -> list.sum() }
assert sum1([2, 2]) == 4

//Free Variables
def myconst = 5
def incbyconst = { num -> num + myconst }
println incbyconst(10)

//IMPLICIT VARIABLES

//Using it
def clos = { println "Hello $it" }
clos.call('world')

//this, owner and delegate
class Class1 {
    def closure = {
        println "---in closure---"
        println this.class.name
        println owner.class.name
        println delegate.class.name
        def nestedClosure = {
            println "---in nestedClosure---"
            println this.class.name
            println owner.class.name
            println delegate.class.name
        }
        nestedClosure()
    }
}

def clos1 = new Class1().closure
clos1()
println ""
println "===changing the delegate==="
clos1.delegate = this
clos1()
println ""

def closure2 = {
    println "--- closure outside the class(in the script)---"
    println this.class.name
    println delegate.class.name
    println owner.class.name

}

closure2()

//Explicit Declaration of Closure
Closure clos2 = { println it }

//Reusing the Method as a Closure
def list = ['Vishal', 'Chris', 'Joseph', 'Jim']
list.each { println it }

String printName(String name) {
    println name
}

list.each(this.&printName)

//Passing a Closure As a Parametr
def list1 = ["Chris", "Joseph", "Jim"]
def sayHello = { println it }
list1.each(sayHello)

//CLOSURES AND COLLECTIONS
//Using the any Method
def anyElement = [1, 2, 3, 4].any { element -> element > 2 }
assert anyElement == true

//Using the collect Method
def doubled = [1, 2, 3, 4].collect { element -> return 2 * element }
assert doubled == [2, 4, 6, 8]

//Using the each Method
[1, 2, 3].each { println it }

//Using the evety Method
def allElements1 = [1, 2, 3, 4].every { element -> element > 1 }
assert allElements1 == false

def allElements2 = [2, 3, 4, 5].every { element -> element > 1 }
assert allElements2 == true

//Using the find Method
def foundElement = [1, 2, 3, 4].find { element -> element > 2 }
assert foundElement == 3

//Using Closure as Key and Value
key1 = { println "closure as key" }
map1 = [(key1): 100]

println map1.get(key1)
println map1[key1]

map1 = [key1: { println "closure as value" }]
map1.key1()

//Currying Closure
def add = { x, y -> return x + y }
def newAdd = add.curry(1)
assert "${newAdd(2)}" == "3"

//Using Trampoline
def factorial

factorial = { n, BigInteger acc = 1 ->
    n == 1 ? acc : factorial.trampoline(n - 1, n * acc)
}.trampoline()

factorial(1000)

//Using Memoization
closure3 = { param1, param2 -> sleep(100); param1 + param2 }

memoizedClosure = closure3.memoize()

def testTime(param1, param2) {
    begin = System.currentTimeMillis()
    memoizedClosure(param1, param2)
    timeElapsed = System.currentTimeMillis()
    println "param1 = $param1, param2 = $param2 time: ${ timeElapsed - begin } ms."
}

testTime(1, 2)
testTime(3, 4)
testTime(1, 2)