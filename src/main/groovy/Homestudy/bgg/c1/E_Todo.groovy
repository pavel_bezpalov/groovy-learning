package Homestudy.bgg.c1

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * Example as a Script
 */
class Todo {
    String name
    String note
}

def todos = [
        new Todo(name: "1", note: "one"),
        new Todo(name: "2", note: "two"),
        new Todo(name: "3", note: "three")
]
todos.each {
    println "$it.name $it.note"
}
/* Result:
1 one
2 two
3 three
 */