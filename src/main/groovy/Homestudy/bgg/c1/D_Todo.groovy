package Homestudy.bgg.c1

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * Example with Groovy Collection Notation and Closure
 */
class D_Todo {
    String name
    String note

    public static void main(String... args) {
        def todos = [
                new D_Todo(name: "1", note: "one"),
                new D_Todo(name: "2", note: "two"),
                new D_Todo(name: "3", note: "three")
        ]
        todos.each {
            println "$it.name $it.note"
        }
    }
}
/* Result:
1 one
2 two
3 three
 */