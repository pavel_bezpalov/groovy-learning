package Homestudy.bgg.c1

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * Simple Example Applying Syntactic Sugar, Implicit Imports, Common Methods and String Features
 */
class C_Todo {
    String name
    String note

    public static void main(String... args) {
        def todos = new ArrayList()
        todos.add(new C_Todo(name: "1", note: "one"))
        todos.add(new C_Todo(name: "2", note: "two"))
        todos.add(new C_Todo(name: "3", note: "three"))

        for (C_Todo todo : todos) {
            println "$todo.name $todo.note"
        }
    }
}
/* Result:
1 one
2 two
3 three
 */