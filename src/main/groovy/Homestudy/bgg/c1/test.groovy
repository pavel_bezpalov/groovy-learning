package Homestudy.bgg.c1

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * Testing Groovy Language Features
 */

// Assertion Example
def list = [1, 2, 'x'] // list of 3 elements
assert list.size() == 3

// Using Annotations for AST Transformation
@Singleton
class SomeSingleton {
//..
}

// Groovy Code to Generate Preceding Tree-like Structure
def builder = new groovy.xml.MarkupBuilder()
builder.employee {
    name 'John Doe'
    gender 'male'
}
/* Result:
<employee>
  <name>John Doe</name>
  <gender>male</gender>
</employee>
 */

// Using Closure
def name = "Chris"
def printClosure = { println "Hello, ${name}" }
printClosure()
name = "Joseph"
printClosure()
/* Result:
Hello, Chris
Hello, Joseph
 */

// Runtime Metaprogramming
String.metaClass.firstUpper = {
    return delegate[0].toUpperCase() + delegate[1..delegate.length() - 1]
}
println "iiii".firstUpper()
/* Result:
Iiii
 */

// JSON Support
import groovy.json.JsonSlurper

def slurper = new JsonSlurper()
def result = slurper.parseText('{"person":{"name":"John Doe","age":40,"cars":["bmw","ford"]}}')
println result.person.name
println result.person.age
println result.person.cars.size()
println result.person.cars[0]
println result.person.cars[1]
/* Result:
John Doe
40
2
bmw
ford
 */

// Support for Lists as a Language Feature
authors = ['Vishal', 'Chris', 'Joseph', 'Jim']
println authors
println authors[0]
/* Result:
[Vishal, Chris, Joseph, Jim]
Vishal
 */

// Object Orientation: Everything is an Object in Groovy
a = 2 + 2
b = 2.plus(2)
assert a == b

// String Interpolation in Groovy
def lastName = "Layka"
def fullname = "Vishal ${lastName} " // string interpolation (also called Gstring)
println fullname
/* Result:
Vishal Layka
 */

// Properties in Groovy
class Employee {
    String name
}

Employee emp = new Employee()
emp.setName("John Doe")
println emp.name
/* Result:
John Doe
 */

// Optional Semicolons and Parentheses
println("hello");
println "hello"
/* Result:
hello
hello
 */

// Return Type and the return Keyword
def greeting() {
    result = "Hello world"
    result
}

println greeting()
/* Result:
Hello world
 */