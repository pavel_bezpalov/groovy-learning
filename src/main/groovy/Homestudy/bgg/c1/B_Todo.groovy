package Homestudy.bgg.c1

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * Simple Example using a GroovyBean
 */
import java.util.List;
import java.util.ArrayList;

class B_Todo {
    String name;
    String note;

    public static void main(String... args) {
        List<B_Todo> todos = new ArrayList<B_Todo>();
        todos.add(new B_Todo(name: "1", note: "one"));
        todos.add(new B_Todo(name: "2", note: "two"));
        todos.add(new B_Todo(name: "3", note: "three"));

        for (B_Todo todo : todos) {
            System.out.println(todo.name + " " + todo.note);
        }
    }
}
/* Result:
1 one
2 two
3 three
 */