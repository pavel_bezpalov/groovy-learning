package Homestudy.bgg.c1

/**
 * Created by Pavel Bezpalov on 21.10.2014.
 * Simple Java Class renamed to .groovy
 */
import java.util.List;
import java.util.ArrayList;

public class A_TodoG {
    private String name;
    private String note;

    public A_TodoG() {}

    public A_TodoG(String name, String note) {
        this.name = name;
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public static void main(String... args) {
        List<A_TodoG> todos = new ArrayList<A_TodoG>();
        todos.add(new A_TodoG("1", "one"));
        todos.add(new A_TodoG("2", "two"));
        todos.add(new A_TodoG("3", "three"));

        for (A_TodoG todo : todos) {
            System.out.println(todo.getName() + " " + todo.getNote());
        }
    }
}
/* Result:
1 one
2 two
3 three
 */