package Homestudy.bgg.c1;

/**
 * Created by Pavel Bezpalov on 20.10.2014.
 * Simple Java Class
 */

import java.util.List;
import java.util.ArrayList;

public class A_Todo {
    private String name;
    private String note;

    public A_Todo() {
    }

    public A_Todo(String name, String note) {
        this.name = name;
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public static void main(String... args) {
        List<A_Todo> todos = new ArrayList<A_Todo>();
        todos.add(new A_Todo("1", "one"));
        todos.add(new A_Todo("2", "two"));
        todos.add(new A_Todo("3", "three"));

        for (A_Todo todo : todos) {
            System.out.println(todo.getName() + " " + todo.getNote());
        }
    }
}
/* Result:
1 one
2 two
3 three
 */